--------------------------------------------------------
--  File created - maandag-januari-07-2019   
--------------------------------------------------------
-- Unable to render PACKAGE DDL for object AFDA.AFDA_ACTIES_TAPI with DBMS_METADATA attempting internal generator.
CREATE 
PACKAGE AFDA.AFDA_ACTIES_TAPI IS
  /**
   * This is the API for the table AFDA_ACTIES.
   *
   * GENERATION OPTIONS
   * <options
   *   generator="tapigen"
   *   generator_version="0.4.1"
   *   generator_action="COMPILE_API"
   *   generated_at="2019-01-07 11:15:42"
   *   generated_by="FVLYMINCKX"
   *   p_table_name="AFDA_ACTIES"
   *   p_enable_insertion_of_rows="TRUE"
   *   p_enable_update_of_rows="TRUE"
   *   p_enable_deletion_of_rows="TRUE"
   *   p_sequence_name="AFDA_ACTIE_ID"/>
   *
   */
  ----------------------------------------
    /***************************************************************
    *                                                                                                                                               *
    *                       TYPES                                                                                                             *
    *                                                                                                                                               *
    ***************************************************************/
    -- Scalar/Column types
            SUBTYPE ACTIE_ID_type IS AFDA_ACTIES.ACTIE_ID%TYPE;
                TYPE t_ACTIE_IDs_type IS TABLE OF ACTIE_ID_type INDEX BY PLS_INTEGER;SUBTYPE PATIENT_NR_type IS AFDA_ACTIES.PATIENT_NR%TYPE;
                TYPE t_PATIENT_NRs_type IS TABLE OF PATIENT_NR_type INDEX BY PLS_INTEGER;SUBTYPE PROF_VOLG_NR_type IS AFDA_ACTIES.PROF_VOLG_NR%TYPE;
                TYPE t_PROF_VOLG_NRs_type IS TABLE OF PROF_VOLG_NR_type INDEX BY PLS_INTEGER;SUBTYPE ACTIE_DT_type IS AFDA_ACTIES.ACTIE_DT%TYPE;
                TYPE t_ACTIE_DTs_type IS TABLE OF ACTIE_DT_type INDEX BY PLS_INTEGER;SUBTYPE BESCHRIJVING_type IS AFDA_ACTIES.BESCHRIJVING%TYPE;
                TYPE t_BESCHRIJVINGs_type IS TABLE OF BESCHRIJVING_type INDEX BY PLS_INTEGER;SUBTYPE RESULTAAT_type IS AFDA_ACTIES.RESULTAAT%TYPE;
                TYPE t_RESULTAATs_type IS TABLE OF RESULTAAT_type INDEX BY PLS_INTEGER;SUBTYPE AFGESLOTEN_IND_type IS AFDA_ACTIES.AFGESLOTEN_IND%TYPE;
                TYPE t_AFGESLOTEN_INDs_type IS TABLE OF AFGESLOTEN_IND_type INDEX BY PLS_INTEGER;SUBTYPE CREATIE_DT_type IS AFDA_ACTIES.CREATIE_DT%TYPE;
                TYPE t_CREATIE_DTs_type IS TABLE OF CREATIE_DT_type INDEX BY PLS_INTEGER;SUBTYPE CREATIE_USER_type IS AFDA_ACTIES.CREATIE_USER%TYPE;
                TYPE t_CREATIE_USERs_type IS TABLE OF CREATIE_USER_type INDEX BY PLS_INTEGER;SUBTYPE WIJZIGING_DT_type IS AFDA_ACTIES.WIJZIGING_DT%TYPE;
                TYPE t_WIJZIGING_DTs_type IS TABLE OF WIJZIGING_DT_type INDEX BY PLS_INTEGER;SUBTYPE WIJZIGING_USER_type IS AFDA_ACTIES.WIJZIGING_USER%TYPE;
                TYPE t_WIJZIGING_USERs_type IS TABLE OF WIJZIGING_USER_type INDEX BY PLS_INTEGER;SUBTYPE COMMENTAAR_type IS AFDA_ACTIES.COMMENTAAR%TYPE;
                TYPE t_COMMENTAARs_type IS TABLE OF COMMENTAAR_type INDEX BY PLS_INTEGER;SUBTYPE EIND_DT_type IS AFDA_ACTIES.EIND_DT%TYPE;
                TYPE t_EIND_DTs_type IS TABLE OF EIND_DT_type INDEX BY PLS_INTEGER;SUBTYPE VERSION_NR_type IS AFDA_ACTIES.VERSION_NR%TYPE;
                TYPE t_VERSION_NRs_type IS TABLE OF VERSION_NR_type INDEX BY PLS_INTEGER;SUBTYPE r_row_type IS AFDA_ACTIES%ROWTYPE;
            TYPE t_rows_type IS TABLE OF r_row_type
        INDEX BY PLS_INTEGER;

    TYPE r_delete_type IS RECORD
    (
        ACTIE_ID ACTIE_ID_type,
        version_nr version_nr_type
    );

    TYPE t_deletes_type IS TABLE OF r_delete_type
        INDEX BY PLS_INTEGER;
    /****************************************************************
    *                                                               *
    *              SECURITY/ACCESS CHECK methods                    *
    *                                                               *
    ****************************************************************/
    -- SECURITYCHECK method
    /** check access to the table has been allowed
    *   used by a table trigger to make sure all DML on AFDA_ACTIES is through the API
    * @return   TRUE -> manipulation is allowed, FALSE -> manipulation is not allowed
    */
    FUNCTION accessallowed
        RETURN BOOLEAN;
    /****************************************************************
    *                                                               *
    *              EXISTS CHECK en INIT methods                     *
    *                                                               *
    ****************************************************************/

  /** check if a row exists by primary key
   * @param    in_ACTIE_ID        primary key, must be NOT NULL
   * @return   row exists -> true else false
   */
  function                 row_exists( in_ACTIE_ID IN ACTIE_ID_type )
  RETURN BOOLEAN;
  ----------------------------------------

    /** check if a row exists by primary key (row exists ->fw.cnst.yes else fw.cnst.no)
     * @param    in_ACTIE_ID        primary key, must be NOT NULL
     * @return   row exists ->fw.cnst.yes else fw.cnst.no
     */
  FUNCTION row_exists_yn( in_ACTIE_ID IN ACTIE_ID_type )
  RETURN fw.types.yesno;
  ----------------------------------------
    /** Init a new row (make row with it's default values)
     *  primary key, ACTIE_ID is returned as NULL, because it will be generated, upon execution of the ins procedure
     */
  FUNCTION init_row
    RETURN r_row_type;
  ----------------------------------------
    /****************************************************************
    *                                                               *
    *                       INSERT ROW methods                      *
    *                                                               *
    ****************************************************************/

    /** insert a row
	 * @param io_ACTIE_ID		ACTIE_ID primary key column, when null it will be generated
	 * @param io_PATIENT_NR		PATIENT_NR
	 * @param io_PROF_VOLG_NR		PROF_VOLG_NR
	 * @param io_ACTIE_DT		ACTIE_DT
	 * @param io_BESCHRIJVING		BESCHRIJVING
	 * @param io_RESULTAAT		RESULTAAT
	 * @param io_AFGESLOTEN_IND		AFGESLOTEN_IND
	 * @param out_CREATIE_DT		CREATIE_DT
	 * @param out_CREATIE_USER		CREATIE_USER
	 * @param out_WIJZIGING_DT		WIJZIGING_DT
	 * @param out_WIJZIGING_USER		WIJZIGING_USER
	 * @param io_COMMENTAAR		COMMENTAAR
	 * @param io_EIND_DT		EIND_DT
	 * @param io_VERSION_NR		VERSION_NR
     */
  PROCEDURE ins(io_ACTIE_ID IN OUT NOCOPY ACTIE_ID_type,io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type,io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type,io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type,io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type,io_RESULTAAT IN OUT NOCOPY RESULTAAT_type,io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type,out_CREATIE_DT OUT NOCOPY CREATIE_DT_type,out_CREATIE_USER OUT NOCOPY CREATIE_USER_type,out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type,out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type,io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type,io_EIND_DT IN OUT NOCOPY EIND_DT_type,io_VERSION_NR IN OUT NOCOPY VERSION_NR_type);
  ----------------------------------------
  /** insert a row
    * @param    in_row        (when in_row.ACTIE_ID is NULL it will be autogenerated)
    * @return   inserted row
    */
  FUNCTION ins (in_row IN r_row_type)
        RETURN r_row_type;
  ----------------------------------------
      /** insert multiple rows, array may not be sparse and not null
      * User needs to check that the number of updated rows is as expected
      *
      * @param    in_rows        array of rows
      * @return   no. records inserted
      */
    FUNCTION bulk_ins (in_rows IN t_rows_type)
        RETURN PLS_INTEGER;
  ----------------------------------------
    /****************************************************************
    *                                                               *
    *                       GET ROW methods                         *
    *                                                               *
    ****************************************************************/

    /** get a row by primary key
     * @param    in_ACTIE_ID        primary key column, must be NOT NULL
     * @param    in_lock      If TRUE the record will be locked (select for update)
     * @return   row
     */
  FUNCTION read_row(in_ACTIE_ID in ACTIE_ID_type, in_lock IN BOOLEAN DEFAULT FALSE)
  RETURN r_row_type;
  ----------------------------------------

    /** get all fields by primary key
     * @param    in_ACTIE_ID               primary key column, must be NOT NULL
	 * @param out_PATIENT_NR	PATIENT_NR
	 * @param out_PROF_VOLG_NR	PROF_VOLG_NR
	 * @param out_ACTIE_DT	ACTIE_DT
	 * @param out_BESCHRIJVING	BESCHRIJVING
	 * @param out_RESULTAAT	RESULTAAT
	 * @param out_AFGESLOTEN_IND	AFGESLOTEN_IND
	 * @param out_CREATIE_DT	CREATIE_DT
	 * @param out_CREATIE_USER	CREATIE_USER
	 * @param out_WIJZIGING_DT	WIJZIGING_DT
	 * @param out_WIJZIGING_USER	WIJZIGING_USER
	 * @param out_COMMENTAAR	COMMENTAAR
	 * @param out_EIND_DT	EIND_DT
	 * @param out_VERSION_NR	VERSION_NR

     * @param    in_lock             If TRUE the record will be locked (select for update)
     */
  PROCEDURE read_row(in_ACTIE_ID in ACTIE_ID_type,out_PATIENT_NR OUT NOCOPY PATIENT_NR_type,out_PROF_VOLG_NR OUT NOCOPY PROF_VOLG_NR_type,out_ACTIE_DT OUT NOCOPY ACTIE_DT_type,out_BESCHRIJVING OUT NOCOPY BESCHRIJVING_type,out_RESULTAAT OUT NOCOPY RESULTAAT_type,out_AFGESLOTEN_IND OUT NOCOPY AFGESLOTEN_IND_type,out_CREATIE_DT OUT NOCOPY CREATIE_DT_type,out_CREATIE_USER OUT NOCOPY CREATIE_USER_type,out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type,out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type,out_COMMENTAAR OUT NOCOPY COMMENTAAR_type,out_EIND_DT OUT NOCOPY EIND_DT_type,out_VERSION_NR OUT NOCOPY VERSION_NR_type, in_lock IN BOOLEAN DEFAULT FALSE);
  ----------------------------------------

    /** get a row by primary key using flashback query
     *  can be used to get the last value of a deleted row
     * @param    in_ACTIE_ID        primary key column, must be NOT NULL
     * @return   row
     */
  FUNCTION read_hist_row(in_ACTIE_ID in ACTIE_ID_type)
  RETURN r_row_type;
  ----------------------------------------

    /****************************************************************
    *                                                               *
    *                       UPDATE ROW methods                      *
    *                                                               *
    ****************************************************************/
    /**
    * update a row in the AFDA_ACTIES table.
    *
    * @param    in_row    in_row.ACTIE_ID, primary key , must not be NULL
    * @return   row
    */
  FUNCTION upd(in_row IN r_row_type)
        RETURN r_row_type;
  ----------------------------------------

    /**
     * update a row in the AFDA_ACTIES table.
     *
     * @param      in_ACTIE_ID             Primary key , must not be NULL 
	 * @param io_PATIENT_NR		io_PATIENT_NR
	 * @param io_PROF_VOLG_NR		io_PROF_VOLG_NR
	 * @param io_ACTIE_DT		io_ACTIE_DT
	 * @param io_BESCHRIJVING		io_BESCHRIJVING
	 * @param io_RESULTAAT		io_RESULTAAT
	 * @param io_AFGESLOTEN_IND		io_AFGESLOTEN_IND
	 * @param out_CREATIE_DT		out_CREATIE_DT
	 * @param out_CREATIE_USER		out_CREATIE_USER
	 * @param out_WIJZIGING_DT		out_WIJZIGING_DT
	 * @param out_WIJZIGING_USER		out_WIJZIGING_USER
	 * @param io_COMMENTAAR		io_COMMENTAAR
	 * @param io_EIND_DT		io_EIND_DT
	 * @param io_VERSION_NR		io_VERSION_NR
	 */
  PROCEDURE upd(in_ACTIE_ID in ACTIE_ID_type, io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type, io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type, io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type, io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type, io_RESULTAAT IN OUT NOCOPY RESULTAAT_type, io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type, out_CREATIE_DT OUT NOCOPY CREATIE_DT_type, out_CREATIE_USER OUT NOCOPY CREATIE_USER_type, out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type, out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type, io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type, io_EIND_DT IN OUT NOCOPY EIND_DT_type, io_VERSION_NR IN OUT NOCOPY VERSION_NR_type);
  ----------------------------------------
    /**
    * update multiple rows, array may not be sparse and not null
    * update is performed taking into account the concurrency control.
    * User needs to check that the number of updated rows is as expected
    *
    * @param      in_rows     array of rows
    * @return     no. records inserted
    */
    FUNCTION bulk_upd (in_rows IN t_rows_type)
        RETURN PLS_INTEGER;
  ----------------------------------------

    /****************************************************************
     *                                                               *
     *                       DELETE ROW methods                      *
     *                                                               *
     ****************************************************************/
    /**
     * delete a row from the AFDA_ACTIES table.
     *
     * @param    in_ACTIE_ID        must be NOT NULL
     */
  PROCEDURE del(in_ACTIE_ID in ACTIE_ID_type,in_VERSION_NR in VERSION_NR_type);
  ----------------------------------------
    /** delete multiple rows, array may not be sparse and not null
     * * User needs to check that the number of deleted rows is as expected
     *
     * @param    in_ACTIE_IDs           array of primary keys
     * @param    in_version_nrs    list of version nummers
     * @return   no. records      deleted
     */
    FUNCTION bulk_del (in_deletes IN t_deletes_type)
        RETURN PLS_INTEGER;
  ----------------------------------------

    /****************************************************************
    *                                                               *
    *                       MERGE ROW methods                       *
    *                                                               *
    ****************************************************************/

    /**
     * insert or update a row in the AFDA_ACTIES table.
     *
     * @param   in_row  update when the primary key is found, insert otherwise
     * @return  row
     */
  FUNCTION ins_or_upd (in_row IN r_row_type)
        RETURN r_row_type;
  ----------------------------------------

    /**
     * insert or update a row in the AFDA_ACTIES table.
     *
     * @param      io_ACTIE_ID             Primary key (auto generated when NULL) 
	 * @param io_ACTIE_ID		io_ACTIE_ID
	 * @param io_PATIENT_NR		io_PATIENT_NR
	 * @param io_PROF_VOLG_NR		io_PROF_VOLG_NR
	 * @param io_ACTIE_DT		io_ACTIE_DT
	 * @param io_BESCHRIJVING		io_BESCHRIJVING
	 * @param io_RESULTAAT		io_RESULTAAT
	 * @param io_AFGESLOTEN_IND		io_AFGESLOTEN_IND
	 * @param out_CREATIE_DT		out_CREATIE_DT
	 * @param out_CREATIE_USER		out_CREATIE_USER
	 * @param out_WIJZIGING_DT		out_WIJZIGING_DT
	 * @param out_WIJZIGING_USER		out_WIJZIGING_USER
	 * @param io_COMMENTAAR		io_COMMENTAAR
	 * @param io_EIND_DT		io_EIND_DT
	 * @param io_VERSION_NR		io_VERSION_NR
	 */
  PROCEDURE ins_or_upd( io_ACTIE_ID IN OUT NOCOPY ACTIE_ID_type, io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type, io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type, io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type, io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type, io_RESULTAAT IN OUT NOCOPY RESULTAAT_type, io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type, out_CREATIE_DT OUT NOCOPY CREATIE_DT_type, out_CREATIE_USER OUT NOCOPY CREATIE_USER_type, out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type, out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type, io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type, io_EIND_DT IN OUT NOCOPY EIND_DT_type, io_VERSION_NR IN OUT NOCOPY VERSION_NR_type);
  ----------------------------------------
END AFDA_ACTIES_TAPI ;

--------------------------------------------------------
--  File created - maandag-januari-07-2019   
--------------------------------------------------------
-- Unable to render PACKAGE BODY DDL for object AFDA.AFDA_ACTIES_TAPI with DBMS_METADATA attempting internal generator.
CREATE 
PACKAGE BODY AFDA.AFDA_ACTIES_TAPI IS
    /**
    * Global logger scope
    *
    */
    FUNCTION gc_scope_prefix
        RETURN VARCHAR2
        DETERMINISTIC
    IS
    BEGIN
        RETURN LOWER ($$plsql_unit) || CHR(46);
    END gc_scope_prefix;

        ----------------------------------------



    /**
    * log all values from a record type parameter
    *
    * @param      in_row      the table row
    * @param      io_params   the logger table
    */
    PROCEDURE log_params (in_row      IN            r_row_type,
                          io_params   IN OUT NOCOPY logger.tab_param)
    IS
        r_row r_row_type not null := in_row;  -- G-8310 validate input parameter size
        t_params logger.tab_param NOT NULL := io_params; -- G-8310 validate input parameter size
    BEGIN
      logger.append_param (p_params=> io_params, p_name=> 'ACTIE_ID', p_val=> in_row.ACTIE_ID);
logger.append_param (p_params=> io_params, p_name=> 'PATIENT_NR', p_val=> in_row.PATIENT_NR);
logger.append_param (p_params=> io_params, p_name=> 'PROF_VOLG_NR', p_val=> in_row.PROF_VOLG_NR);
logger.append_param (p_params=> io_params, p_name=> 'ACTIE_DT', p_val=> in_row.ACTIE_DT);
logger.append_param (p_params=> io_params, p_name=> 'BESCHRIJVING', p_val=> in_row.BESCHRIJVING);
logger.append_param (p_params=> io_params, p_name=> 'RESULTAAT', p_val=> in_row.RESULTAAT);
logger.append_param (p_params=> io_params, p_name=> 'AFGESLOTEN_IND', p_val=> in_row.AFGESLOTEN_IND);
logger.append_param (p_params=> io_params, p_name=> 'COMMENTAAR', p_val=> in_row.COMMENTAAR);
logger.append_param (p_params=> io_params, p_name=> 'EIND_DT', p_val=> in_row.EIND_DT);
logger.append_param (p_params=> io_params, p_name=> 'VERSION_NR', p_val=> in_row.VERSION_NR);

    END log_params;

    ----------------------------------------
    /**
    * log all values from a record type parameter
    *
    * @param      in_row      the table row
    * @param      io_params   the logger table
    */
    PROCEDURE log_params (in_rows      IN            t_rows_type,
                          io_params   IN OUT NOCOPY logger.tab_param)
    IS
        t_rows t_rows_type NOT NULL := in_rows; -- G-8310 validate input parameter size
        t_params logger.tab_param NOT NULL := io_params; -- G-8310 validate input parameter size
        l_idx PLS_INTEGER := in_rows.first;
    BEGIN
        <<append_loop>>
       while (l_idx is not null)
       loop
            logger.append_param (p_params   => io_params,
                                 p_name     => 'index of in_rows',
                                 p_val      => l_idx);
            log_params (in_row   => in_rows(l_idx),
                                 io_params => io_params);
           l_idx := in_rows.next(l_idx);
       end loop append_loop;
    END log_params;

    ----------------------------------------
    /**
    * log all values from a record type parameter
    *
    * @param      in_deletes      keys & versions of records that are to be deleted
    * @param      io_params   the logger table
    */
    PROCEDURE log_params (in_deletes      IN            t_deletes_type,
                          io_params   IN OUT NOCOPY logger.tab_param)
    IS
        t_deletes t_deletes_type NOT NULL := in_deletes; -- G-8310 validate input parameter size
        t_params logger.tab_param NOT NULL := io_params; -- G-8310 validate input parameter size
        l_idx PLS_INTEGER := t_deletes.first;
    BEGIN
        <<append_loop>>
       while (l_idx is not null)
       loop
            logger.append_param (p_params   => io_params,
                                 p_name     => 'key '||TO_CHAR(l_idx),
                                 p_val      => t_deletes(l_idx).ACTIE_ID);
            logger.append_param (p_params   => io_params,
                                 p_name     => 'version_nr '||TO_CHAR(l_idx),
                                 p_val      => t_deletes(l_idx).version_nr);
           l_idx := t_deletes.next(l_idx);
       end loop append_loop;
    END log_params;

    ----------------------------------------

  ----------------------------------------
            PROCEDURE enableaccess
    IS
    BEGIN
        fw.ctx.SET (p_name => 'AFDA_ACTIES', p_value => fw.cnst.yes);
    END enableaccess;

        ----------------------------------------

    PROCEDURE disableaccess
    IS
    BEGIN
        fw.ctx.SET (p_name => 'AFDA_ACTIES', p_value => fw.cnst.no );
    END disableaccess;

    ----------------------------------------

    FUNCTION accessallowed
        RETURN BOOLEAN
    IS
    BEGIN
        RETURN COALESCE (fw.ctx.get (p_name => 'AFDA_ACTIES'), fw.cnst.no ) =
               fw.cnst.yes;
    END accessallowed;
  ----------------------------------------
  FUNCTION row_exists( in_ACTIE_ID IN ACTIE_ID_type )
  RETURN BOOLEAN
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'row_exists';
    t_params   logger.tab_param;
    l_count PLS_INTEGER;
    co_ACTIE_ID CONSTANT ACTIE_ID_type not null := in_ACTIE_ID;
  BEGIN
    logger.append_param (p_params=> t_params, p_name=> 'in_actie_id', p_val=> in_actie_id);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);

    SELECT SUM(1)
    INTO l_count FROM AFDA_ACTIES WHERE "ACTIE_ID" = co_ACTIE_ID;
    logger.LOG (p_text => 'END', p_scope => co_scope);
    RETURN l_count <> 0;
    exception
        when     NO_DATA_FOUND
        THEN
            logger.log_error(p_text=> 'record niet gevonden', p_scope => co_scope, p_extra => SQLERRM, p_params => t_params);
            disableaccess;
            raise;
        when others
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope => co_scope, p_extra => null, p_params => t_params);
            disableaccess;
            raise;

  END row_exists;
  ----------------------------------------
  FUNCTION row_exists_yn( in_ACTIE_ID IN ACTIE_ID_type )
  RETURN fw.types.yesno
  IS
    co_ACTIE_ID CONSTANT ACTIE_ID_type not null := in_ACTIE_ID;
  BEGIN
    RETURN case when row_exists( in_ACTIE_ID => co_ACTIE_ID )
             then fw.cnst.yes
             else fw.cnst.no
           end;
  END row_exists_yn;
  ----------------------------------------
  FUNCTION init_row
    RETURN r_row_type
  IS
    co_scope   CONSTANT fw.logs.scope_type := gc_scope_prefix || 'init_row';
    r_row r_row_type;
  BEGIN
        -- start logging
        logger.LOG (p_text => 'START', p_scope => co_scope);

        -- set default values
        r_row.AFGESLOTEN_IND := 'N' ;
        r_row.CREATIE_DT := SYSDATE ;
        r_row.CREATIE_USER := USER ;
        r_row.VERSION_NR := 0 ;
        

        -- end logging
        logger.LOG (p_text => 'END', p_scope => co_scope);

    RETURN r_row;
  END init_row;
  ----------------------------------------
  PROCEDURE ins(io_ACTIE_ID IN OUT NOCOPY ACTIE_ID_type,io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type,io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type,io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type,io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type,io_RESULTAAT IN OUT NOCOPY RESULTAAT_type,io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type,out_CREATIE_DT OUT NOCOPY CREATIE_DT_type,out_CREATIE_USER OUT NOCOPY CREATIE_USER_type,out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type,out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type,io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type,io_EIND_DT IN OUT NOCOPY EIND_DT_type,io_VERSION_NR IN OUT NOCOPY VERSION_NR_type)
  IS
    r_row r_row_type;
  BEGIN r_row.actie_id := io_actie_id;r_row.patient_nr := io_patient_nr;r_row.prof_volg_nr := io_prof_volg_nr;r_row.actie_dt := io_actie_dt;r_row.beschrijving := io_beschrijving;r_row.resultaat := io_resultaat;r_row.afgesloten_ind := io_afgesloten_ind;r_row.commentaar := io_commentaar;r_row.eind_dt := io_eind_dt;r_row.version_nr := io_version_nr;

    r_row := ins(in_row => r_row);

    io_actie_id := r_row.actie_id;io_patient_nr := r_row.patient_nr;io_prof_volg_nr := r_row.prof_volg_nr;io_actie_dt := r_row.actie_dt;io_beschrijving := r_row.beschrijving;io_resultaat := r_row.resultaat;io_afgesloten_ind := r_row.afgesloten_ind;out_creatie_dt := r_row.creatie_dt;out_creatie_user := r_row.creatie_user;out_wijziging_dt := r_row.wijziging_dt;out_wijziging_user := r_row.wijziging_user;io_commentaar := r_row.commentaar;io_eind_dt := r_row.eind_dt;io_version_nr := r_row.version_nr;
  END ins;
  ----------------------------------------
  FUNCTION ins (in_row IN r_row_type)
        RETURN r_row_type
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'ins';
    t_params   logger.tab_param;
    r_row r_row_type NOT NULL := in_row;
  BEGIN
    log_params (in_row      => in_row, io_params  => t_params);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);

    enableaccess;

    -- validate the row
    logger.LOG (p_text => 'validating row', p_scope => co_scope);
    AFDA_ACTIES_hook.val (io_row => r_row);
    -- pre insert
    logger.LOG (p_text => 'pre insert row', p_scope => co_scope);
    AFDA_ACTIES_hook.pre_ins (io_new_row => r_row);
        r_row.ACTIE_ID := COALESCE(r_row.ACTIE_ID, AFDA_ACTIE_ID.nextval);
    r_row.creatie_user := fw.utils.wijziging_user;
    r_row.creatie_dt := fw.utils.wijziging_dt;
    r_row.wijziging_user := NULL;
    r_row.wijziging_dt := NULL;
    r_row.version_nr := 0;
    INSERT INTO AFDA_ACTIES
    VALUES r_row RETURN ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR INTO r_row;

    -- post insert
    AFDA_ACTIES_hook.post_ins ( in_new_row => r_row);

    disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
            return       r_row;
    exception
        when     DUP_VAL_ON_INDEX
        THEN
            logger.log_error(p_text=> 'duplicate values', p_scope => co_scope, p_extra => SQLERRM, p_params => t_params);
            disableaccess;
            raise;
        when others
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope => co_scope, p_extra => null, p_params => t_params);
            disableaccess;
            raise;
  END ins;
  ----------------------------------------
  FUNCTION bulk_ins (in_rows IN t_rows_type)
        RETURN PLS_INTEGER
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'bulk_ins';
    t_params   logger.tab_param;
    e_bulk_error      EXCEPTION;
    PRAGMA exception_init(e_bulk_error, -24381);
    t_rows t_rows_type NOT NULL := in_rows;
    t_rows_inserted t_rows_type;
    l_err_cnt_lower_bound PLS_INTEGER := 1;
    l_err_cnt_upper_bound PLS_INTEGER := 0;
        t_err_params   logger.tab_param;
    co_creatie_user   CONSTANT creatie_user_type
                                       := fw.utils.wijziging_user ;
    co_creatie_dt     CONSTANT creatie_user_type := fw.utils.wijziging_dt;

  BEGIN
    log_params (in_rows      => t_rows, io_params  => t_params);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);
    fw.assert(p_condition=> t_rows.COUNT = t_rows.LAST,
    p_message => 'array can not be sparse',p_module => co_scope);
    enableaccess;

    -- validate the rows
    logger.LOG (p_text => 'bulk val rows', p_scope => co_scope);
    AFDA_ACTIES_hook.bulk_val (in_rows => t_rows);
    -- pre insert
    logger.LOG (p_text => 'bulk pre_ins rows', p_scope => co_scope);
    AFDA_ACTIES_hook.bulk_pre_ins (io_new_rows => t_rows);

    FORALL i IN INDICES OF t_rows SAVE EXCEPTIONS
       INSERT INTO AFDA_ACTIES ( ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR )
    VALUES (COALESCE(t_rows(i).ACTIE_ID, AFDA_ACTIE_ID.nextval) -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).PATIENT_NR -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).PROF_VOLG_NR -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).ACTIE_DT -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).BESCHRIJVING -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).RESULTAAT -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).AFGESLOTEN_IND -- NOSONAR: avoid G-7110 this is not a function call
,co_creatie_dt,co_creatie_user,null,null,t_rows(i).COMMENTAAR -- NOSONAR: avoid G-7110 this is not a function call
,t_rows(i).EIND_DT -- NOSONAR: avoid G-7110 this is not a function call
,0) RETURN ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR BULK COLLECT INTO t_rows_inserted;

    -- post insert
    AFDA_ACTIES_hook.bulk_post_ins (in_new_rows => t_rows_inserted);

    disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
            return t_rows_inserted.COUNT;
    EXCEPTION
    WHEN e_bulk_error THEN
        l_err_cnt_upper_bound := SQL%BULK_EXCEPTIONS.COUNT; -- NOSONAR: avoid G-4140
        <<bulk_error_loop>>
        FOR i IN l_err_cnt_lower_bound..l_err_cnt_upper_bound
        LOOP
            log_params (in_row      => t_rows(SQL%BULK_EXCEPTIONS(i).error_index) -- NOSONAR: avoid G-7110 this is not a function call

            , io_params  => t_err_params);
            logger.log_error(p_text=> SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE) -- NOSONAR: avoid G-7110 this is not a function call
,  p_scope => co_scope, p_extra => 'index of item = '||SQL%BULK_EXCEPTIONS(i).error_index -- NOSONAR: avoid G-7110 this is not a function call

            , p_params => t_err_params);
        END LOOP bulk_error_loop;
        disableaccess;
        RAISE;
    WHEN OTHERS
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope => co_scope, p_extra => null, p_params => t_params);
            disableaccess;
            RAISE;

  END bulk_ins;
  ----------------------------------------
  FUNCTION read_row(in_ACTIE_ID in ACTIE_ID_type, in_lock IN BOOLEAN DEFAULT FALSE)
  RETURN r_row_type
  IS
    l_ACTIE_ID CONSTANT ACTIE_ID_type not null := in_ACTIE_ID;
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'read_row';
    t_params   logger.tab_param;
    CURSOR c_cur( p_ACTIE_ID IN ACTIE_ID_type ) IS
      SELECT ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR FROM AFDA_ACTIES WHERE ACTIE_ID = p_ACTIE_ID;
    CURSOR c_cur_lock( p_ACTIE_ID IN ACTIE_ID_type ) IS
      SELECT ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR FROM AFDA_ACTIES WHERE ACTIE_ID = p_ACTIE_ID FOR UPDATE;
    r_row r_row_type;
  BEGIN logger.append_param (p_params=> t_params, p_name=> 'in_actie_id', p_val=> in_actie_id); logger.append_param (p_params=> t_params, p_name=> 'in_lock', p_val=> in_lock);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);
        -- read the row
        IF in_lock
        THEN
            OPEN c_cur_lock( p_ACTIE_ID => l_ACTIE_ID );
            FETCH c_cur_lock INTO r_row;
            CLOSE c_cur_lock;
        ELSE
            OPEN c_cur( p_ACTIE_ID => l_ACTIE_ID );
            FETCH c_cur INTO r_row;
            CLOSE c_cur;
        END IF;logger.LOG (p_text => 'END', p_scope => co_scope);
    RETURN r_row;
  END read_row;
  ----------------------------------------
  PROCEDURE read_row(in_ACTIE_ID in ACTIE_ID_type,out_PATIENT_NR OUT NOCOPY PATIENT_NR_type,out_PROF_VOLG_NR OUT NOCOPY PROF_VOLG_NR_type,out_ACTIE_DT OUT NOCOPY ACTIE_DT_type,out_BESCHRIJVING OUT NOCOPY BESCHRIJVING_type,out_RESULTAAT OUT NOCOPY RESULTAAT_type,out_AFGESLOTEN_IND OUT NOCOPY AFGESLOTEN_IND_type,out_CREATIE_DT OUT NOCOPY CREATIE_DT_type,out_CREATIE_USER OUT NOCOPY CREATIE_USER_type,out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type,out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type,out_COMMENTAAR OUT NOCOPY COMMENTAAR_type,out_EIND_DT OUT NOCOPY EIND_DT_type,out_VERSION_NR OUT NOCOPY VERSION_NR_type, in_lock IN BOOLEAN DEFAULT FALSE)
  IS
    r_row r_row_type;
  BEGIN
    r_row := read_row(in_ACTIE_ID => in_ACTIE_ID, in_lock => in_lock); out_PATIENT_NR := r_row.PATIENT_NR;out_PROF_VOLG_NR := r_row.PROF_VOLG_NR;out_ACTIE_DT := r_row.ACTIE_DT;out_BESCHRIJVING := r_row.BESCHRIJVING;out_RESULTAAT := r_row.RESULTAAT;out_AFGESLOTEN_IND := r_row.AFGESLOTEN_IND;out_CREATIE_DT := r_row.CREATIE_DT;out_CREATIE_USER := r_row.CREATIE_USER;out_WIJZIGING_DT := r_row.WIJZIGING_DT;out_WIJZIGING_USER := r_row.WIJZIGING_USER;out_COMMENTAAR := r_row.COMMENTAAR;out_EIND_DT := r_row.EIND_DT;out_VERSION_NR := r_row.VERSION_NR;
  END read_row;
  ----------------------------------------
  FUNCTION read_hist_row(in_ACTIE_ID in ACTIE_ID_type)
  RETURN r_row_type
  IS
    co_ACTIE_ID CONSTANT ACTIE_ID_type not null := in_ACTIE_ID;
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'read_hist_row';
    t_params   logger.tab_param;
    r_row r_row_type; 
  BEGIN logger.append_param (p_params=> t_params, p_name=> 'in_actie_id', p_val=> in_actie_id);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);logger.LOG (p_text => 'no flashback archive for table AFDA_ACTIES', p_scope => co_scope);logger.LOG (p_text => 'END', p_scope => co_scope);
    RETURN r_row;
  END read_hist_row;
  ----------------------------------------
  FUNCTION upd(in_row IN r_row_type)
        RETURN r_row_type
  IS
    r_old_row   r_row_type;
    r_row   r_row_type NOT NULL := in_row;
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'upd';
    t_params   logger.tab_param;
  BEGIN
    log_params (in_row      => in_row, io_params  => t_params);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);

    enableaccess;
    r_old_row := read_row (in_ACTIE_ID => r_row.ACTIE_ID, in_lock => TRUE);
    IF r_old_row.ACTIE_ID is null
    THEN
        logger.LOG (p_text => 'skip update, row does not exist', p_scope => co_scope);
    ELSE

    -- check concurrency control
    IF r_old_row.version_nr != r_row.version_nr
    THEN
        fw.excp.throwconcurrency;
    END IF;

    IF fw.utils.equals(p_1=> r_old_row.PATIENT_NR, p_2=> in_row.PATIENT_NR)
AND fw.utils.equals(p_1=> r_old_row.PROF_VOLG_NR, p_2=> in_row.PROF_VOLG_NR)
AND fw.utils.equals(p_1=> r_old_row.ACTIE_DT, p_2=> in_row.ACTIE_DT)
AND fw.utils.equals(p_1=> r_old_row.BESCHRIJVING, p_2=> in_row.BESCHRIJVING)
AND fw.utils.equals(p_1=> r_old_row.RESULTAAT, p_2=> in_row.RESULTAAT)
AND fw.utils.equals(p_1=> r_old_row.AFGESLOTEN_IND, p_2=> in_row.AFGESLOTEN_IND)
AND fw.utils.equals(p_1=> r_old_row.COMMENTAAR, p_2=> in_row.COMMENTAAR)
AND fw.utils.equals(p_1=> r_old_row.EIND_DT, p_2=> in_row.EIND_DT)
      THEN
        logger.LOG (p_text => 'skip update, no column changes', p_scope => co_scope);

    ELSE
    -- validate the row
    logger.LOG (p_text => 'validating row', p_scope => co_scope);
    AFDA_ACTIES_hook.val (io_row => r_row);
    -- pre update
    AFDA_ACTIES_hook.pre_upd (io_new_row   => r_row,
                                        in_old_row   => r_old_row);
        UPDATE AFDA_ACTIES
        SET  PATIENT_NR = r_row.PATIENT_NR, PROF_VOLG_NR = r_row.PROF_VOLG_NR, ACTIE_DT = r_row.ACTIE_DT, BESCHRIJVING = r_row.BESCHRIJVING, RESULTAAT = r_row.RESULTAAT, AFGESLOTEN_IND = r_row.AFGESLOTEN_IND, WIJZIGING_DT = fw.utils.wijziging_dt, WIJZIGING_USER = fw.utils.wijziging_user, COMMENTAAR = r_row.COMMENTAAR, EIND_DT = r_row.EIND_DT, VERSION_NR = VERSION_NR + 1 WHERE ACTIE_ID = r_row.ACTIE_ID  AND VERSION_NR = in_row.VERSION_NR RETURNING  ACTIE_ID, PATIENT_NR, PROF_VOLG_NR, ACTIE_DT, BESCHRIJVING, RESULTAAT, AFGESLOTEN_IND, CREATIE_DT, CREATIE_USER, WIJZIGING_DT, WIJZIGING_USER, COMMENTAAR, EIND_DT,VERSION_NR INTO r_row.ACTIE_ID,r_row.PATIENT_NR,r_row.PROF_VOLG_NR,r_row.ACTIE_DT,r_row.BESCHRIJVING,r_row.RESULTAAT,r_row.AFGESLOTEN_IND,r_row.CREATIE_DT,r_row.CREATIE_USER,r_row.WIJZIGING_DT,r_row.WIJZIGING_USER,r_row.COMMENTAAR,r_row.EIND_DT,r_row.VERSION_NR;


                IF SQL%ROWCOUNT = 0
                THEN                                      -- concurrency error
                    fw.excp.throwconcurrency;
                END IF;

    -- post update
    AFDA_ACTIES_hook.post_upd (in_new_row   => r_row,
                                        in_old_row   => r_old_row);

    END IF;
    END IF;
    disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
    RETURN r_row;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX
        THEN
            logger.log_error(p_text=> 'duplicate values', p_scope => co_scope, p_extra => SQLERRM, p_params => t_params);
            disableaccess;
            RAISE;
        WHEN OTHERS
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope => co_scope, p_extra => null, p_params => t_params);
            disableaccess;
            RAISE;
  END upd;
  ----------------------------------------
  PROCEDURE upd(in_ACTIE_ID in ACTIE_ID_type, io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type, io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type, io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type, io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type, io_RESULTAAT IN OUT NOCOPY RESULTAAT_type, io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type, out_CREATIE_DT OUT NOCOPY CREATIE_DT_type, out_CREATIE_USER OUT NOCOPY CREATIE_USER_type, out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type, out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type, io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type, io_EIND_DT IN OUT NOCOPY EIND_DT_type, io_VERSION_NR IN OUT NOCOPY VERSION_NR_type)
  IS
    r_row r_row_type;
  BEGIN r_row.actie_id := in_actie_id; r_row.patient_nr := io_patient_nr; r_row.prof_volg_nr := io_prof_volg_nr; r_row.actie_dt := io_actie_dt; r_row.beschrijving := io_beschrijving; r_row.resultaat := io_resultaat; r_row.afgesloten_ind := io_afgesloten_ind;     r_row.commentaar := io_commentaar; r_row.eind_dt := io_eind_dt; r_row.version_nr := io_version_nr;

    r_row := upd(in_row => r_row);

     io_patient_nr := r_row.patient_nr; io_prof_volg_nr := r_row.prof_volg_nr; io_actie_dt := r_row.actie_dt; io_beschrijving := r_row.beschrijving; io_resultaat := r_row.resultaat; io_afgesloten_ind := r_row.afgesloten_ind; out_creatie_dt := r_row.creatie_dt; out_creatie_user := r_row.creatie_user; out_wijziging_dt := r_row.wijziging_dt; out_wijziging_user := r_row.wijziging_user; io_commentaar := r_row.commentaar; io_eind_dt := r_row.eind_dt; io_version_nr := r_row.version_nr;
  END upd;
  ----------------------------------------
    FUNCTION bulk_upd (in_rows IN t_rows_type)
        RETURN PLS_INTEGER
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'bulk_upd';
    t_params   logger.tab_param;
    t_new_rows t_rows_type NOT NULL := in_rows;
    t_old_rows   t_rows_type;
    t_updated_rows t_rows_type;

    e_bulk_error          EXCEPTION;
    PRAGMA EXCEPTION_INIT (e_bulk_error, -24381);
    l_err_cnt_lower_bound PLS_INTEGER := 1;
    l_err_cnt_upper_bound PLS_INTEGER := 0;
    t_err_params   logger.tab_param;

  BEGIN
    log_params (in_rows      => t_new_rows, io_params  => t_params);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);
fw.assert(p_condition=> t_new_rows.COUNT = t_new_rows.LAST,
    p_message => 'array can not be sparse',p_module => co_scope);

    enableaccess;


        -- populate t_old_rows

      SELECT oud.*
      BULK COLLECT INTO t_old_rows
    FROM TABLE(t_new_rows) nieuw
    LEFT JOIN AFDA_ACTIES oud ON nieuw.ACTIE_ID = oud.ACTIE_ID
    FOR UPDATE;

    <<update_candidates_loop>>
    FOR i in 1..t_new_rows.COUNT
    LOOP
            -- concurrency error
            IF t_old_rows(i).ACTIE_ID IS NULL
            OR  t_new_rows(i).version_nr != t_old_rows(i).version_nr -- NOSONAR: avoid G-7110 this is not a function call
            THEN
                fw.excp.throwconcurrency;
            END IF;
    END LOOP update_candidates_loop;


    -- validate the rows
    logger.LOG (p_text => 'validating row', p_scope => co_scope);
    AFDA_ACTIES_hook.bulk_val (in_rows => t_new_rows);
    -- pre update rows
    AFDA_ACTIES_hook.bulk_pre_upd (io_new_rows   => t_new_rows,
                                     in_old_rows   => t_old_rows);



    <<update_candidates_loop>>
    FOR i in 1..t_new_rows.COUNT
        LOOP
            -- concurrency error, opnieuw controleren omdat bulk_val en bulk_pre_upd de collections kunnen aanpassen
            IF t_old_rows(i).ACTIE_ID IS NULL
            OR  t_new_rows(i).version_nr != t_old_rows(i).version_nr -- NOSONAR: avoid G-7110 this is not a function call
            THEN
                fw.excp.throwconcurrency;
            END IF;
            IF      fw.utils.equals_jn (p_1 => t_new_rows(i).PATIENT_NR, p_2 => t_old_rows(i).PATIENT_NR) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).PROF_VOLG_NR, p_2 => t_old_rows(i).PROF_VOLG_NR) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).ACTIE_DT, p_2 => t_old_rows(i).ACTIE_DT) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).BESCHRIJVING, p_2 => t_old_rows(i).BESCHRIJVING) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).RESULTAAT, p_2 => t_old_rows(i).RESULTAAT) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).AFGESLOTEN_IND, p_2 => t_old_rows(i).AFGESLOTEN_IND) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).COMMENTAAR, p_2 => t_old_rows(i).COMMENTAAR) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).EIND_DT, p_2 => t_old_rows(i).EIND_DT) = fw.cnst.no AND fw.utils.equals_jn (p_1 => t_new_rows(i).VERSION_NR, p_2 => t_old_rows(i).VERSION_NR) = fw.cnst.no 
            THEN
                -- update is niet nodig
                t_new_rows(i).ACTIE_ID := NULL;
            END IF;
        END LOOP update_candidates_loop;

    FORALL i IN 1..t_new_rows.count
        UPDATE AFDA_ACTIES
        SET  PATIENT_NR = t_new_rows(i).PATIENT_NR -- NOSONAR: avoid G-7110 this is not a function call
, PROF_VOLG_NR = t_new_rows(i).PROF_VOLG_NR -- NOSONAR: avoid G-7110 this is not a function call
, ACTIE_DT = t_new_rows(i).ACTIE_DT -- NOSONAR: avoid G-7110 this is not a function call
, BESCHRIJVING = t_new_rows(i).BESCHRIJVING -- NOSONAR: avoid G-7110 this is not a function call
, RESULTAAT = t_new_rows(i).RESULTAAT -- NOSONAR: avoid G-7110 this is not a function call
, AFGESLOTEN_IND = t_new_rows(i).AFGESLOTEN_IND -- NOSONAR: avoid G-7110 this is not a function call
, WIJZIGING_DT = fw.utils.wijziging_dt, WIJZIGING_USER = fw.utils.wijziging_user, COMMENTAAR = t_new_rows(i).COMMENTAAR -- NOSONAR: avoid G-7110 this is not a function call
, EIND_DT = t_new_rows(i).EIND_DT -- NOSONAR: avoid G-7110 this is not a function call
, VERSION_NR = VERSION_NR + 1 WHERE ACTIE_ID = t_new_rows(i).ACTIE_ID -- NOSONAR: avoid G-7110 this is not a function call
            RETURN ACTIE_ID, 
PATIENT_NR, 
PROF_VOLG_NR, 
ACTIE_DT, 
BESCHRIJVING, 
RESULTAAT, 
AFGESLOTEN_IND, 
CREATIE_DT, 
CREATIE_USER, 
WIJZIGING_DT, 
WIJZIGING_USER, 
COMMENTAAR, 
EIND_DT, 
VERSION_NR
            BULK COLLECT INTO t_updated_rows;

    -- post update
    AFDA_ACTIES_hook.bulk_post_upd (io_new_rows   => t_updated_rows,
                                      in_old_rows   => t_old_rows);

    disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
    RETURN t_updated_rows.COUNT;
    EXCEPTION
    WHEN e_bulk_error THEN
        l_err_cnt_upper_bound := SQL%BULK_EXCEPTIONS.COUNT; -- NOSONAR: avoid G-4140
        <<bulk_error_loop>>
        FOR i IN l_err_cnt_lower_bound..l_err_cnt_upper_bound
        LOOP
            log_params (in_row      => t_new_rows(SQL%BULK_EXCEPTIONS(i).error_index) -- NOSONAR: avoid G-7110 this is not a function call

            , io_params  => t_err_params);
            logger.log_error(p_text=> SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE) -- NOSONAR: avoid G-7110 this is not a function call
, p_scope=> co_scope, p_extra=> 'index of item = '||SQL%BULK_EXCEPTIONS(i).error_index -- NOSONAR: avoid G-7110 this is not a function call

            , p_params=> t_err_params);
        END LOOP bulk_error_loop;
        disableaccess;
        RAISE;
        WHEN OTHERS
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope=> co_scope, p_extra=> null, p_params=> t_params);
            disableaccess;
            RAISE;
  END bulk_upd;
  ----------------------------------------
  PROCEDURE del(in_ACTIE_ID in ACTIE_ID_type,in_VERSION_NR in VERSION_NR_type)
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'del';
    t_params   logger.tab_param;
    r_row      r_row_type;
  BEGIN logger.append_param (p_params=> t_params, p_name=> 'in_actie_id', p_val=> in_actie_id);logger.append_param (p_params=> t_params, p_name=> 'in_version_nr', p_val=> in_version_nr);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);

          enableaccess;
        r_row := read_row (in_ACTIE_ID => in_ACTIE_ID, in_lock => TRUE);

        --concurrency check
        IF r_row.version_nr != in_version_nr
        THEN
            fw.excp.throwconcurrency;
        END IF;

        -- pre delete
        AFDA_ACTIES_hook.pre_del (in_old_row => r_row);

        DELETE FROM AFDA_ACTIES
        WHERE  ACTIE_ID = in_ACTIE_ID AND VERSION_NR = in_VERSION_NR;

        IF SQL%ROWCOUNT = 0 AND row_exists(in_ACTIE_ID => in_ACTIE_ID)
        THEN                                              -- concurrency error
            fw.excp.throwconcurrency;
        END IF;

        -- prost delete
        AFDA_ACTIES_hook.post_del (in_old_row => r_row);
        disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
    EXCEPTION
        WHEN PROGRAM_ERROR
        THEN
            logger.log_error(p_text=> 'PROGRAM_ERROR', p_scope => co_scope, p_extra => SQLERRM, p_params => t_params);
            disableaccess;
            RAISE;
        WHEN OTHERS
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope=> co_scope, p_extra=> null, p_params=> t_params);
            disableaccess;
            RAISE;
  END del;
  ----------------------------------------
  FUNCTION bulk_del (in_deletes IN t_deletes_type)
        RETURN PLS_INTEGER
  IS
    co_scope    CONSTANT fw.logs.scope_type := gc_scope_prefix || 'bulk_del';
    t_params   logger.tab_param;
    l_aantal_deleted   PLS_INTEGER := 0;
    t_old_deletes t_deletes_type NOT NULL := in_deletes;
    l_index PLS_INTEGER := 0;
  BEGIN
    log_params (in_deletes      => t_old_deletes, io_params  => t_params);
    logger.LOG (p_text => 'START', p_scope => co_scope, p_params => t_params);
    enableaccess;

    -- pre insert
    logger.LOG (p_text => 'bulk pre_del rows', p_scope => co_scope);
    AFDA_ACTIES_hook.bulk_pre_del (io_deletes => t_old_deletes);

    FORALL i IN INDICES OF t_old_deletes
       DELETE FROM AFDA_ACTIES
    WHERE ACTIE_ID = t_old_deletes(i).ACTIE_ID -- NOSONAR: avoid G-7110 this is not a function call
    AND version_nr = t_old_deletes(i).version_nr; -- NOSONAR: avoid G-7110 this is not a function call

    l_aantal_deleted := SQL%ROWCOUNT; -- NOSONAR: avoid G-4140

    IF l_aantal_deleted != t_old_deletes.COUNT
    THEN
        fw.excp.throwconcurrency;
    END IF;



    -- post insert
    AFDA_ACTIES_hook.bulk_post_del (in_deletes => t_old_deletes);

    disableaccess;
    logger.LOG (p_text => 'END', p_scope => co_scope);
            return l_aantal_deleted;
    EXCEPTION
    WHEN PROGRAM_ERROR
        THEN
            logger.log_error(p_text=> 'PROGRAM_ERROR', p_scope => co_scope, p_extra => SQLERRM, p_params => t_params);
            disableaccess;
            RAISE;
    WHEN OTHERS
        THEN
            logger.log_error(p_text=> 'Unhandled Exception', p_scope => co_scope, p_extra => null, p_params => t_params);
            disableaccess;
            RAISE;

  END bulk_del;
  ----------------------------------------
  FUNCTION ins_or_upd (in_row IN r_row_type)
        RETURN r_row_type
  IS
    r_row r_row_type;
  BEGIN
    IF row_exists(in_ACTIE_ID => in_row.ACTIE_ID )
    THEN
        r_row := upd( in_row => in_row );
    ELSE
        r_row := ins( in_row => in_row );
    END IF;
    RETURN r_row;
  END ins_or_upd;
  ----------------------------------------
  PROCEDURE ins_or_upd( io_ACTIE_ID IN OUT NOCOPY ACTIE_ID_type, io_PATIENT_NR IN OUT NOCOPY PATIENT_NR_type, io_PROF_VOLG_NR IN OUT NOCOPY PROF_VOLG_NR_type, io_ACTIE_DT IN OUT NOCOPY ACTIE_DT_type, io_BESCHRIJVING IN OUT NOCOPY BESCHRIJVING_type, io_RESULTAAT IN OUT NOCOPY RESULTAAT_type, io_AFGESLOTEN_IND IN OUT NOCOPY AFGESLOTEN_IND_type, out_CREATIE_DT OUT NOCOPY CREATIE_DT_type, out_CREATIE_USER OUT NOCOPY CREATIE_USER_type, out_WIJZIGING_DT OUT NOCOPY WIJZIGING_DT_type, out_WIJZIGING_USER OUT NOCOPY WIJZIGING_USER_type, io_COMMENTAAR IN OUT NOCOPY COMMENTAAR_type, io_EIND_DT IN OUT NOCOPY EIND_DT_type, io_VERSION_NR IN OUT NOCOPY VERSION_NR_type)
  IS
    r_row r_row_type;
  BEGIN  r_row.actie_id := io_actie_id; r_row.patient_nr := io_patient_nr; r_row.prof_volg_nr := io_prof_volg_nr; r_row.actie_dt := io_actie_dt; r_row.beschrijving := io_beschrijving; r_row.resultaat := io_resultaat; r_row.afgesloten_ind := io_afgesloten_ind;     r_row.commentaar := io_commentaar; r_row.eind_dt := io_eind_dt; r_row.version_nr := io_version_nr;

    r_row := ins_or_upd(in_row => r_row);

     io_actie_id := r_row.actie_id; io_patient_nr := r_row.patient_nr; io_prof_volg_nr := r_row.prof_volg_nr; io_actie_dt := r_row.actie_dt; io_beschrijving := r_row.beschrijving; io_resultaat := r_row.resultaat; io_afgesloten_ind := r_row.afgesloten_ind; out_creatie_dt := r_row.creatie_dt; out_creatie_user := r_row.creatie_user; out_wijziging_dt := r_row.wijziging_dt; out_wijziging_user := r_row.wijziging_user; io_commentaar := r_row.commentaar; io_eind_dt := r_row.eind_dt; io_version_nr := r_row.version_nr;
  END ins_or_upd;
  ----------------------------------------
END AFDA_ACTIES_TAPI ;

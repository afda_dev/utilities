--------------------------------------------------------
--  File created - maandag-januari-07-2019   
--------------------------------------------------------
-- Unable to render PACKAGE BODY DDL for object AFDA.AFDA_ACTIES_HOOK with DBMS_METADATA attempting internal generator.
CREATE 
PACKAGE BODY AFDA.AFDA_ACTIES_HOOK 

AS
    PROCEDURE val (io_row IN OUT AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END val;


    PROCEDURE pre_ins (io_new_row IN OUT AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END pre_ins;


    PROCEDURE post_ins (in_new_row IN AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END post_ins;


    PROCEDURE pre_upd (io_new_row   IN OUT AFDA_ACTIES_TAPI.r_row_type,
                       in_old_row   IN     AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END pre_upd;


    PROCEDURE post_upd (in_new_row   IN AFDA_ACTIES_TAPI.r_row_type,
                        in_old_row   IN AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END post_upd;


    PROCEDURE pre_del (in_old_row IN AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END pre_del;


    PROCEDURE post_del (in_old_row IN AFDA_ACTIES_TAPI.r_row_type)
    IS
    BEGIN
        NULL;
    END post_del;

    PROCEDURE bulk_val (in_rows IN AFDA_ACTIES_TAPI.t_rows_type)
    IS
    BEGIN
        NULL;
    END bulk_val;

    PROCEDURE bulk_pre_ins (io_new_rows IN OUT AFDA_ACTIES_TAPI.t_rows_type)
    IS
    BEGIN
        NULL;
    END bulk_pre_ins;

    PROCEDURE bulk_post_ins (in_new_rows IN AFDA_ACTIES_TAPI.t_rows_type)
    IS
    BEGIN
        NULL;
    END bulk_post_ins;

    PROCEDURE bulk_pre_upd (
        io_new_rows   IN OUT AFDA_ACTIES_TAPI.t_rows_type,
        in_old_rows   IN     AFDA_ACTIES_TAPI.t_rows_type)
    IS
    BEGIN
        NULL;
    END bulk_pre_upd;

    PROCEDURE bulk_post_upd (io_new_rows   IN AFDA_ACTIES_TAPI.t_rows_type,
                             in_old_rows   IN AFDA_ACTIES_TAPI.t_rows_type)
    IS
    BEGIN
        NULL;
    END bulk_post_upd;

    PROCEDURE bulk_pre_del (io_deletes IN AFDA_ACTIES_TAPI.t_deletes_type)
    IS
    BEGIN
        NULL;
    END bulk_pre_del;

    PROCEDURE bulk_post_del (in_deletes IN AFDA_ACTIES_TAPI.t_deletes_type)
    IS
    BEGIN
        NULL;
    END bulk_post_del;
END AFDA_ACTIES_hook;
